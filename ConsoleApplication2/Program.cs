﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace ConsoleApplication2
{
    class Program
    {
        static Socket ClientSocket;
        static Socket ServerSocket;

        static void Main(string[] args)
        {
            ClientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            IPEndPoint ipendPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8);
            ClientSocket.BeginConnect(ipendPoint, new AsyncCallback(ConnectCallback), null);
            Console.ReadKey();
        }

        private static void ConnectCallback(IAsyncResult ar)
        {
            ClientSocket.EndConnect(ar);
            Console.WriteLine("Connected to {0}", ClientSocket.RemoteEndPoint);
        }
    }
}
