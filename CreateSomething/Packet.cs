﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CreateSomething
{
    class Packet
    {
        MemoryStream ms = new MemoryStream();

        public void Write(string data)
        {
            byte[] buffer = Encoding.ASCII.GetBytes(data);

            ms.Write(buffer, 0, buffer.Length);
        }

        public byte[] GetBytes()
        {
            return ms.ToArray();
        }
    }
}