﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace ConsoleApplication1
{
    class Program
    {
        static Socket ServerSocket;
        static Socket ClientSocket;
        static byte[] buffer;

        static void Main(string[] args)
        {
            ServerSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            IPEndPoint ipendPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8);

            ServerSocket.Bind(ipendPoint);
            ServerSocket.Listen(0);

            Console.WriteLine("Listening at {0}", ipendPoint);
            ServerSocket.BeginAccept(new AsyncCallback(AcceptCallback), null);

            Console.ReadKey();
        }

        private static void AcceptCallback(IAsyncResult ar)
        {
            ClientSocket = ServerSocket.EndAccept(ar);

            Console.WriteLine("New connection from: {0}", ClientSocket.RemoteEndPoint);
            ServerSocket.BeginAccept(new AsyncCallback(AcceptCallback), null);
        }
    }
}
